# TP 4

## Build

On Linux, use:

```sh
$ gcc -o tp main.c login.c stock.c menu.c
$
```

## Run

```sh
$ ./tp
Enter your username: KESSAS
Enter your password: Nadir
3 items loaded successfully

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [99] Exit
```

### View Stock


```sh
        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [99] Exit

Please enter your selection: 1

        Reference: 1
        Brand: Dell
        RAM: 1
        Disk: 2048
        OS: Windows7
        Coleur: blue
        Count: 2

        Reference: 2
        Brand: IBM
        RAM: 2
        Disk: 2048
        OS: Windows10
        Coleur: beige
        Count: 2

        Reference: 3
        Brand: Apple
        RAM: 3
        Disk: 2048
        OS: OSX
        Coleur: metal
        Count: 3

        Reference: 4
        Brand: Apple
        RAM: 4
        Disk: 4096
        OS: OSX
        Coleur: white
        Count: 5

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [99] Exit
```

### Search Stock

```sh

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [99] Exit

Please enter your selection: 4

Please enter the brand of the computer to search: App

Below are the matches:

        Reference: 3
        Brand: Apple
        RAM: 3
        Disk: 2048
        OS: OSX
        Coleur: metal
        Count: 3

        Reference: 4
        Brand: Apple
        RAM: 4
        Disk: 4096
        OS: OSX
        Coleur: white
        Count: 5

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [99] Exit

Please enter your selection: 4

Please enter the brand of the computer to search: Tes

No matches found

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [99] Exit
```

### Modify Item

```sh

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [99] Exit

Please enter your selection: 5

Enter the item reference to update or choose 99 to return to main menu: 4
        Marque [Apple]: Apple
        RAM [16384]: 16384
        Disque [4096]: 2048
        System [OSX]: OSX
        Coleur [white]: metallic
        Qantity [5]: 4

Item with reference '4' successfully updated

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [99] Exit

Please enter your selection: 4

Please enter the brand of the computer to search: App

Below are the matches:

        Reference: 3
        Brand: Apple
        RAM: 3
        Disk: 2048
        OS: OSX
        Coleur: metal
        Count: 3

        Reference: 4
        Brand: Apple
        RAM: 4
        Disk: 2048
        OS: OSX
        Coleur: metallic
        Count: 4
```

### Remove Item

```sh

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [99] Exit

Please enter your selection: 3

Enter the item reference to remove or choose 99 to return to main menu: 2
Item with reference '2' successfully removed

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [99] Exit

Please enter your selection: 1

        Reference: 1
        Brand: Dell
        RAM: 1
        Disk: 2048
        OS: Windows7
        Coleur: blue
        Count: 2

        Reference: 3
        Brand: Apple
        RAM: 3
        Disk: 2048
        OS: OSX
        Coleur: metal
        Count: 3

        Reference: 4
        Brand: Apple
        RAM: 4
        Disk: 2048
        OS: OSX
        Coleur: metallic
        Count: 4
```

### Add new Item

```sh

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [99] Exit

Please enter your selection: 2

Adding a new item
        Reference: 1
An item with reference '1' already exists, please try again
        Reference: 2
        Marque: Lenovo
        RAM: 4096
        Disque: 2048
        System: Windows10
        Couleur: white
        Qantity: 3
New item successfully added

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [99] Exit

Please enter your selection: 1

        Reference: 1
        Brand: Dell
        RAM: 1
        Disk: 2048
        OS: Windows7
        Coleur: blue
        Count: 2

        Reference: 3
        Brand: Apple
        RAM: 3
        Disk: 2048
        OS: OSX
        Coleur: metal
        Count: 3

        Reference: 4
        Brand: Apple
        RAM: 4
        Disk: 2048
        OS: OSX
        Coleur: metallic
        Count: 4

        Reference: 2
        Brand: Lenovo
        RAM: 2
        Disk: 2048
        OS: Windows10
        Coleur: white
        Count: 3

```

### Mis a Jour

```sh

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [06] Mis a Jour
        [99] Exit

Please enter your selection: 6

Please enter the name of the Mis-a-Jour file: maj.txt

3 items were updated
```

### Exit

```sh

        [01] View Stock
        [02] Add new Item
        [03] Remove Item
        [04] Search Items
        [05] Modify Item
        [99] Exit

Please enter your selection: 99
Good bye
 $ cat stock.txt 
        1       Dell    4096    2048    Windows7        blue    2
        3       Apple   4096    2048    OSX     metal   3
        4       Apple   16384   2048    OSX     metallic        4
        2       Lenovo  4096    2048    Windows10       white   3
```
