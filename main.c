#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "types.h"

int main(int argc, char **argv)
{

    // Initial state of the Stock before loading anything
    Stock stock = {Count : 0};

    // Do the login
    if (!login())
    {
        return -1;
    }

    // Load the contents of STOCK_FILENAME and exit of the loading failed
    loadStockTSV(&stock);
    if (stock.Count < 0)
    {
        return -2;
    }

    // Print how many items were loaded
    if (stock.Count)
        printf("%d items loaded successfully\n", stock.Count);
    else
        printf("No stock available\n");

    // Do the main menu
    menu(stock);

    // Print exit message
    printf("Good bye\n");
    return 0;
}
