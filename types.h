
#define MAX_LOGIN_ATTEMPTS 3
#define MAX_STRING_SIZE 64
#define STOCK_FILENAME "stock.txt"

// This struct represents one user account (username & password)
typedef struct User
{
    char username[MAX_STRING_SIZE];
    char password[MAX_STRING_SIZE];
} User;

// This struct represents a Computer and all its attributes
typedef struct Computer
{
    int reference;
    char marque[20];
    int ram;
    int disque;
    char systemeexp[15];
    char couleur[15];
    int quantite;

} Computer;

// Stock is the list of all the computer in stock and their count
typedef struct Stock
{
    Computer *Items;
    int Count;
} Stock;

// This struct represents a mis-a-jour update
typedef struct Update
{
    int reference;
    int quantite;

} Update;

// These are the function prototype definitions
char login();

void loadStock(Stock *stock);
void loadStockTSV(Stock *stock);

int saveStock(Stock stock);
int saveStockTSV(Stock stock);
int updateStock(Stock *stock);
int updateItem(Stock *stock, Update u);

void menu(Stock stock);
void removeItem(Stock *stock);
void showStock(Stock stock);
void addItem(Stock *stock);
void modifyItem(Stock *stock);
void searchItem(Stock stock);

Computer *searchByReference(Stock stock, int reference);
Stock searchByMarque(Stock stock, char *marque);
void printItem(Computer item);