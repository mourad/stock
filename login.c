#include <stdio.h>
#include <string.h>

#include "types.h"

// This is the list of username/passwords allowed to login to the system
User Users[] = {
    {username : "mourad", password : "kessas"},
    {username : "KESSAS", password : "Nadir"},
};

/* login is used to authenticate the user based on the contents of the "Users" array */
char login()
{

    char username[MAX_STRING_SIZE];
    char password[MAX_STRING_SIZE];

    for (int i = 0; i < MAX_LOGIN_ATTEMPTS; i++)
    {
        printf("Enter your username: ");
        scanf("%s", username);
        printf("Enter your password: ");
        scanf("%s", password);

        for (int i = 0; i < sizeof(Users) / sizeof(User); i++)
        {
            if ((strcmp(Users[i].username, username) == 0) && (strcmp(Users[i].password, password) == 0))
                return 1;
        }

        printf("Invalid username or password, please try again (attempt %d of %d)\n", i + 1, MAX_LOGIN_ATTEMPTS);
    }

    printf("Maximum login attempts reached\n");
    return 0;
}
