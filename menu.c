#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"

/* menu is the main menu dialog, used to prompt the user for all available options */
void menu(Stock stock)
{

    char options[6][MAX_STRING_SIZE] = {
        "View Stock",
        "Add new Item",
        "Remove Item",
        "Search Items",
        "Modify Item",
        "Mis a Jour",
    };
    int choice;

    do
    {
        printf("\n");
        for (int i = 0; i < sizeof(options) / MAX_STRING_SIZE; i++)
        {
            printf("\t[%02d] %s\n", i + 1, options[i]);
        }
        printf("\t[99] Exit\n");

        printf("\nPlease enter your selection: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            showStock(stock);
            break;
        case 2:
            addItem(&stock);
            break;
        case 3:
            removeItem(&stock);
            break;
        case 4:
            searchItem(stock);
            break;
        case 5:
            modifyItem(&stock);
            break;
        case 6:
            updateStock(&stock);
            break;
        case 99:
            return;
        default:
            printf("Invalid selection, please try again\n");
        }

    } while (choice != 99);
}

/* updateStock is used to combine the stock in the main file with the new mis-a-jour file */
int updateStock(Stock *stock)
{
    char filename[MAX_STRING_SIZE];
    FILE *fp;
    int updated = 0;

    printf("\nPlease enter the name of the Mis-a-Jour file: ");
    scanf("%s", filename);

    if (!(fp = fopen(filename, "r")))
    {
        printf("\nInvalid filename '%s'\n", filename);
        return updated;
    }

    do
    {
        Update c;
        if (fscanf(fp, "%d\t%d\n", &c.reference, &c.quantite) == EOF)
            break;

        updated += updateItem(stock, c);

    } while (!feof(fp));
    fclose(fp);

    // Save the merged stock back to the stock file
    saveStockTSV(*stock);

    printf("\n%d items were updated\n", updated);

    return updated;
}

/* modifyItem is used to prompt the user for the new fields to use to update an existing item.  The item is searched by reference */
void modifyItem(Stock *stock)
{
    int id;
    do
    {
        printf("\nEnter the item reference to update or choose 99 to return to main menu: ");
        scanf("%d", &id);

        if (id == 99)
            return;

        for (int i = 0; i < stock->Count; i++)
        {
            Computer c = stock->Items[i];
            if (c.reference == id)
            {

                printf("\tMarque [%s]: ", c.marque);
                scanf("%s", c.marque);

                printf("\tRAM [%d]: ", c.ram);
                scanf("%d", &c.ram);

                printf("\tDisque [%d]: ", c.disque);
                scanf("%d", &c.disque);

                printf("\tSystem [%s]: ", c.systemeexp);
                scanf("%s", c.systemeexp);

                printf("\tColeur [%s]: ", c.couleur);
                scanf("%s", c.couleur);

                printf("\tQantity [%d]: ", c.quantite);
                scanf("%d", &c.quantite);

                stock->Items[i] = c;
                saveStockTSV(*stock);
                printf("\nItem with reference '%d' successfully updated\n", id);
                return;
            }
        }

        printf("\nItem with reference '%d' does not exist, please try again or choose 99 to return to main menu\n", id);

    } while (1);
}

/* removeItem is used to delete one item from the stock.  It provides the dialog to ask the user for a reference ID of the matching item to delete */
void removeItem(Stock *stock)
{
    int id;
    do
    {
        printf("\nEnter the item reference to remove or choose 99 to return to main menu: ");
        scanf("%d", &id);

        if (id == 99)
            return;

        for (int i = 0; i < stock->Count; i++)
        {
            if (stock->Items[i].reference == id)
            {
                for (int j = i; j < stock->Count - 1; j++)
                {
                    stock->Items[j] = stock->Items[j + 1];
                }
                stock->Count--;
                saveStockTSV(*stock);
                printf("Item with reference '%d' successfully removed\n", id);
                return;
            }
        }

        printf("\nItem with reference '%d' does not exist, please try again or choose 99 to return to main menu\n", id);

    } while (1);
}

/* searchItem is used to produce the menu dialog prompting the user to provide a search brand (marque) and prints the matching results */
void searchItem(Stock stock)
{

    char search[MAX_STRING_SIZE];

    printf("\nPlease enter the brand of the computer to search: ");
    scanf("%s", search);

    Stock match = searchByMarque(stock, search);

    if (!match.Count)
    {
        printf("\nNo matches found\n");
        return;
    }

    printf("\nBelow are the matches:\n");

    for (int i = 0; i < match.Count; i++)
        printItem(match.Items[i]);
}

/* addItem is used to add a new item to the stock, it will generate all the prompts to ask the user for the details of the new item */
void addItem(Stock *stock)
{
    Computer c; // = {*count + 1, "Dell", 16, 2048, "Windows 7", "beige", 5};

    printf("\nAdding a new item\n");

    while (1)
    {
        printf("\tReference: ");
        scanf("%d", &c.reference);
        if (!searchByReference(*stock, c.reference))
        {
            break;
        }
        printf("An item with reference '%d' already exists, please try again\n", c.reference);
    }

    printf("\tMarque: ");
    scanf("%s", c.marque);

    printf("\tRAM: ");
    scanf("%d", &c.ram);

    printf("\tDisque: ");
    scanf("%d", &c.disque);

    printf("\tSystem: ");
    scanf("%s", c.systemeexp);

    printf("\tCouleur: ");
    scanf("%s", c.couleur);

    printf("\tQantity: ");
    scanf("%d", &c.quantite);

    if (!stock->Items)
        stock->Items = (Computer *)malloc(sizeof(Computer));
    else
        stock->Items = realloc(stock->Items, sizeof(Computer) * (stock->Count + 1));

    stock->Items[stock->Count] = c;
    stock->Count++;
    saveStockTSV(*stock);

    printf("New item successfully added\n");
}
