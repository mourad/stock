#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "types.h"

/* loadStock reads the contents of the stock file (defined by STOCK_FILENAME) and updates the stock accordingly */
void loadStock(Stock *stock)
{
    FILE *fp;

    if (!(fp = fopen(STOCK_FILENAME, "rb")))
    {
        return;
    }

    do
    {
        Computer c;
        if (!fread(&c, sizeof(Computer), 1, fp))
            break;

        stock->Items = realloc(stock->Items, sizeof(Computer) * (stock->Count + 1));
        stock->Items[stock->Count] = c;
        stock->Count++;
    } while (!feof(fp));
    fclose(fp);
    return;
}

/* loadStockTSV reads the contents of the stock file in tab-separated format (defined by STOCK_FILENAME) and updates the stock accordingly */
void loadStockTSV(Stock *stock)
{
    FILE *fp;

    if (!(fp = fopen(STOCK_FILENAME, "r")))
    {
        return;
    }

    do
    {
        Computer c;
        if (fscanf(fp, "%d\t%s\t%d\t%d\t%s\t%s\t%d\n", &c.reference, c.marque, &c.ram, &c.disque, c.systemeexp, c.couleur, &c.quantite) == EOF)
            break;

        stock->Items = realloc(stock->Items, sizeof(Computer) * (stock->Count + 1));
        stock->Items[stock->Count] = c;
        stock->Count++;
    } while (!feof(fp));
    fclose(fp);
    return;
}

/* saveStock writes the contents of the stock object back to a binary file (defined by STOCK_FILENAME) */
int saveStock(Stock stock)
{
    FILE *fp;
    if (!(fp = fopen(STOCK_FILENAME, "wb")))
    {
        printf("Failed to write stock file %s\n", STOCK_FILENAME);
        return 0;
    }

    for (int i = 0; i < stock.Count; i++)
    {
        if (!fwrite(&stock.Items[i], sizeof(Computer), 1, fp))
        {
            printf("Failed to write item #%d\n", i + 1);
        }
    }

    fclose(fp);
    return 1;
}

/* saveStockTSV writes the contents of the stock object back to a tab-separated file (defined by STOCK_FILENAME) */
int saveStockTSV(Stock stock)
{
    FILE *fp;
    if (!(fp = fopen(STOCK_FILENAME, "w")))
    {
        printf("Failed to write stock file %s\n", STOCK_FILENAME);
        return 0;
    }

    for (int i = 0; i < stock.Count; i++)
    {
        Computer c = stock.Items[i];

        if (!fprintf(fp, "%d\t%s\t%d\t%d\t%s\t%s\t%d\n", c.reference, c.marque, c.ram, c.disque, c.systemeexp, c.couleur, c.quantite))
        {
            printf("Failed to write item #%d\n", i + 1);
        }
    }

    fclose(fp);
    return 1;
}

/* searchByReference searches the stock for exactly one item that match the "reference" field */
Computer *searchByReference(Stock stock, int reference)
{
    for (int i = 0; i < stock.Count; i++)
    {
        if (stock.Items[i].reference == reference)
            return &stock.Items[i];
    }

    return 0;
}

/* searchByMarque searches the stock for one or more items that match the "marque" field */
Stock searchByMarque(Stock stock, char *marque)
{
    Computer *items = (Computer *)malloc(sizeof(Computer));
    Stock matches = {Count : 0, Items : items};

    for (int i = 0; i < stock.Count; i++)
    {
        if (strncmp(stock.Items[i].marque, marque, strlen(marque)) == 0)
        {
            matches.Items = realloc(matches.Items, sizeof(Computer) * (stock.Count + 1));
            matches.Items[matches.Count] = stock.Items[i];
            matches.Count++;
        }
    }
    return matches;
}

/* printItem pretty-print the contents of one item */
void printItem(Computer item)
{
    printf("\n\tReference: %d\n\tBrand: %s\n\tRAM: %d\n\tDisk: %d\n\tOS: %s\n\tColeur: %s\n\tCount: %d\n", item.reference, item.marque, item.reference, item.disque, item.systemeexp, item.couleur, item.quantite);
}

/* showStock is used to display the contents of the stock from memory */
void showStock(Stock stock)
{
    if (!stock.Count)
    {
        printf("\nNo stock available\n");
        return;
    }

    for (int i = 0; i < stock.Count; i++)
    {
        printItem(stock.Items[i]);
    }
}
