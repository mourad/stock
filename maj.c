#include <stdio.h>
#include <stdlib.h>

#define MAX_STRING_LEN 64
#define STOCK_FILENAME "stock.txt"
#define MAJ_FILENAME "maj.txt"
#define OUTPUT_FILENAME "output.txt"

// This struct represents a Computer and all its attributes
typedef struct Computer
{
    int reference;
    char marque[20];
    int ram;
    int disque;
    char systemeexp[15];
    char couleur[15];
    int quantite;

} Computer;

// This struct represents a Mis-a-Jour Stock update
typedef struct Update
{
    int reference;
    int quantite;

} Update;

// Stock is the list of all the computer in stock and their count
typedef struct Stock
{
    Computer *Items;
    int Count;
} Stock;

typedef struct MAJ
{
    Update *Items;
    int Count;
} MAJ;

/* loadStock reads the contents of the stock file in tab-separated format (defined by STOCK_FILENAME) and updates the stock accordingly */
int loadStock(Stock *stock)
{
    FILE *fp;

    if (!(fp = fopen(STOCK_FILENAME, "r")))
    {
        return 1;
    }

    do
    {
        Computer c;
        if (fscanf(fp, "%d\t%s\t%d\t%d\t%s\t%s\t%d\n", &c.reference, c.marque, &c.ram, &c.disque, c.systemeexp, c.couleur, &c.quantite) == EOF)
            break;

        stock->Items = realloc(stock->Items, sizeof(Computer) * (stock->Count + 1));
        stock->Items[stock->Count] = c;
        stock->Count++;
    } while (!feof(fp));
    fclose(fp);
    return 0;
}

int loadMAJ(MAJ *maj)
{
    FILE *fp;

    if (!(fp = fopen(MAJ_FILENAME, "r")))
    {
        return 1;
    }

    do
    {
        Update c;
        if (fscanf(fp, "%d\t%d\n", &c.reference, &c.quantite) == EOF)
            break;

        maj->Items = realloc(maj->Items, sizeof(Computer) * (maj->Count + 1));
        maj->Items[maj->Count] = c;
        maj->Count++;
    } while (!feof(fp));
    fclose(fp);
    return 0;
}

void updateItem(Stock *stock, Update *update)
{
    for (int i = 0; i < stock->Count; i++)
    {
        if (stock->Items[i].reference == update->reference)
        {
            stock->Items[i].quantite += update->quantite;
            return;
        }
    }
    printf(" - Item with reference #%d is not found in the Stock file, skipping\n", update->reference);
}

/* saveStockMAJ writes the contents of the stock object back to a tab-separated file (defined by OUTPUT_FILENAME) */
int saveStockMAJ(Stock stock)
{
    FILE *fp;
    if (!(fp = fopen(OUTPUT_FILENAME, "w")))
    {
        printf("Failed to write output file %s\n", OUTPUT_FILENAME);
        return 0;
    }

    for (int i = 0; i < stock.Count; i++)
    {
        Computer c = stock.Items[i];

        if (!fprintf(fp, "%d\t%s\t%d\t%d\t%s\t%s\t%d\n", c.reference, c.marque, c.ram, c.disque, c.systemeexp, c.couleur, c.quantite))
        {
            printf("Failed to write item #%d\n", i + 1);
        }
    }

    fclose(fp);
    return 1;
}

int main(int argc, char **argv)
{
    Stock stock = {Count : 0};
    MAJ maj = {Count : 0};

    // Load the contents of STOCK_FILENAME and exit of the loading failed
    if (loadStock(&stock) < 0)
    {
        printf("Error loading data from '%s'\n", STOCK_FILENAME);
        return 1;
    }

    // Load the contents of MAJ_FILENAME and exit of the loading failed
    if (loadMAJ(&maj) < 0)
    {
        printf("Error loading data from '%s'\n", MAJ_FILENAME);
        return 1;
    }

    // Merge MAJ and Stock into one
    for (int i = 0; i < maj.Count; i++)
    {
        updateItem(&stock, &maj.Items[i]);
    }

    saveStockMAJ(stock);

    return 0;
}